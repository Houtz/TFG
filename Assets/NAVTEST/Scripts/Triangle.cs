﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triangle
{
    public int area;
    public int index;
    public Vector3 refPoint; //Barycentre by default
    public float fCost;
    public float gCost;
    public Triangle chain;
    



    public Triangle(int area, int index, Vector3 pA, Vector3 pB, Vector3 pC)
    {
        this.area = area;
        this.index = index;
        CalculateBarycentre(pA, pB, pC);
    }

    private void CalculateBarycentre(Vector3 pA, Vector3 pB, Vector3 pC)
    {
        refPoint.x = (pA.x + pB.x + pC.x) / 3;
        refPoint.y = (pA.y + pB.y + pC.y) / 3;
        refPoint.z = (pA.z + pB.z + pC.z) / 3;
    }

}