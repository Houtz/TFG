﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class NAV : MonoBehaviour
{
    // movement
    public Transform goal;
    private NavMeshAgent agent;
    private int destPoint = 0;

    //C
    Triangle start;
    Triangle end;
    List<Triangle> triangleList;
    NavMeshTriangulation navT;
    Triangle t;
    AStar astar;
    float radius;

    Vector3 travelPoint;
    Vector3 lastPos;
    bool outOfBound = false;
    bool direct = false;

    Vector3 v1;
    Vector3 v2;
    Vector3 v1Last;
    Vector3 v2Last;
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = agent.speed;// / Random.Range(1,5);
        //agent.autoBraking = false;
        lastPos = this.transform.position;

        triangleList = new List<Triangle>();
        navT = NavMesh.CalculateTriangulation();
        int area = 0;
        Vector3 A, B, C;
        for (int i = 0; i < navT.indices.Length; i += 3)
        {
            A = navT.vertices[navT.indices[i]];
            B = navT.vertices[navT.indices[i + 1]];
            C = navT.vertices[navT.indices[i + 2]];
            Triangle t = new Triangle(area, i, A, B, C);
            ++area;
            triangleList.Add(t);
            if (start == null && Tools.TriangulationBarycentre(A, B, C, this.transform.position))
            {
                start = t;
                t.refPoint = this.transform.position;
            }
            else if (end == null && Tools.TriangulationBarycentre(A, B, C, goal.position))
            {
                end = t;
                t.refPoint = goal.position;
            }


        }

        astar = new AStar(start, end, navT, triangleList);
        astar.FindPath();
        List<Triangle> trianglePath = astar.path;
        end.refPoint = goal.position;
        destPoint = 0;
        GotoNextPoint();

    }
    void GotoNextPoint()
    {
        if (travelPoint != end.refPoint)
        {
            if (astar.path.Count != 0  || destPoint != -1 || destPoint < astar.path.Count)
            {
                NavMeshHit hit;
                NextTriangle(out v1, out v2);
                travelPoint = ClosestPointPortal(v1, v2, this.transform.position);
              /*  while (!agent.Raycast(travelPoint, out hit))
                {
                    direct = true;
                    destPoint++;
                    NextTriangle(out v1, out v2);
                    travelPoint = ClosestPointPortal(v1, v2, this.transform.position);
                }*/

                agent.destination = travelPoint;
            }
        }

    }
    

    void FixedUpdate()
    {
        
        if (travelPoint != end.refPoint )
        {
            if (agent.remainingDistance < Random.Range(0.2f, 0.30f))
            {
                if (direct) destPoint = Tools.CurrentTrianglePath(navT, astar.path, this.transform.position);
                GotoNextPoint();
                direct = false;
            }
            else
            {
                NavMeshHit hit;
                bool rayCast = agent.Raycast(end.refPoint, out hit);
                if (!rayCast)
                {
                    travelPoint = end.refPoint;
                    direct = true;
                }
                else if (outOfBound || direct)
                {
                    if (destPoint != -1 && destPoint + 1 < astar.path.Count)
                    {
                        NextTriangle(out v1, out v2);

                        //Debug.DrawRay(this.transform.position, Vector3.up * 10, Color.red);

                        travelPoint = ClosestPointPortal(v1, v2, this.transform.position);

                        direct = true;

                    }
                }
                
            }
        }
        agent.destination = travelPoint;
        this.transform.LookAt(travelPoint);
        v1Last = v1;
        v2Last = v2;
        /*
        for(int a = 0; a < astar.path.Count; ++a )
        {
            Debug.DrawLine(navT.vertices[navT.indices[astar.path[a].index]], navT.vertices[navT.indices[astar.path[a].index] + 1], Color.red);
            Debug.DrawLine(navT.vertices[navT.indices[astar.path[a].index]], navT.vertices[navT.indices[astar.path[a].index] + 2], Color.red);
            Debug.DrawLine(navT.vertices[navT.indices[astar.path[a].index]+2], navT.vertices[navT.indices[astar.path[a].index] + 1], Color.red);
        }
        /*Debug.DrawLine(v1, v2, Color.red);
        Debug.DrawRay(v1, Vector3.up * 10, Color.red);
        Debug.DrawRay(v2, Vector3.up * 10, Color.red);
        Debug.DrawRay(travelPoint, Vector3.up * 10, Color.green);
        Debug.DrawLine(this.transform.position, travelPoint, Color.green);*/

    }
    private void NextTriangle( out Vector3 v1, out Vector3 v2  )
    {
        v1 = v1Last;
        v2 = v2Last;
        if (destPoint + 1 < astar.path.Count && destPoint != -1)
        {
            if (Tools.sameVertex(navT.vertices[navT.indices[astar.path[destPoint].index]], astar.path[destPoint + 1], navT))
            {
                v1 = navT.vertices[navT.indices[astar.path[destPoint].index]];
            }
            if (Tools.sameVertex(navT.vertices[navT.indices[astar.path[destPoint].index + 1]], astar.path[destPoint + 1], navT))
            {
                if (v1 == v1Last) v1 = navT.vertices[navT.indices[astar.path[destPoint].index + 1]];
                else v2 = navT.vertices[navT.indices[astar.path[destPoint].index + 1]];
            }
            if (Tools.sameVertex(navT.vertices[navT.indices[astar.path[destPoint].index + 2]], astar.path[destPoint + 1], navT))
            {
                if (v2 == v2Last) v2 = navT.vertices[navT.indices[astar.path[destPoint].index + 2]];
            }

        }
    }
    Vector3 ClosestPointPortal(Vector3 vA, Vector3 vB, Vector3 point)
    {
        Vector3 ClosestPoint;
        vA = Vector3.Lerp(vA, vB, 0.2f);
        vB = Vector3.Lerp(vB, vA, 0.2f);
        Vector3 aux1 = point - vA;
        Vector3 aux2 = (vB - vA).normalized;

        var d = Vector3.Distance(vA, vB);
        var t = Vector3.Dot(aux2, aux1);

        if (t <= 0) ClosestPoint = vA;
        else if (t >= d) ClosestPoint = vB;
        else
        {
            Vector3 aux3 = aux2 * t;
            ClosestPoint = vA + aux3;
        }

        if (!(Vector3.Dot(aux2, (ClosestPoint - vB).normalized) < 0f && Vector3.Dot((vA - vB).normalized, (ClosestPoint - vA).normalized) < 0f))
        {
            outOfBound = true;
            ClosestPoint = FurthestVertex(vA, vB, point);
        }
        else
        {
            outOfBound = false;
        }

        return ClosestPoint;
    }
    private Vector3 FurthestVertex( Vector3 vA, Vector3 vB, Vector3 point )
    {
        Vector3 ret;
        if (Mathf.Abs(Vector3.Distance(vA, point)) >= Mathf.Abs(Vector3.Distance(vB, point)))
        {
            ret = vA;
        }
        else
        {
            ret = vB;
        }
        return ret;
    }
}
    