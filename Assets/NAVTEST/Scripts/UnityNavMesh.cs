﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UnityNavMesh : MonoBehaviour {

    public Transform goal;
    // Use this for initialization
    NavMeshAgent agent;
    void Start () {
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        agent.destination = goal.position;
	}
}
